import express from 'express';
import productsRoutes from './routes/products.routes.js';
import cartsRoutes from './routes/carts.routes.js';

const PORT = 8080;
const app = express();
const router = express.Router();


app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// app.use('/api/products', productsRoutes);
// app.use('/api/carts', cartsRoutes);


router.use('/products', productsRoutes);
router.use('/carts', cartsRoutes);

app.use('/api', router);

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
