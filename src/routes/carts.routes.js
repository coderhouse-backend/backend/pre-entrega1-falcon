import { Router } from 'express';

import cartManager from '../manager/cart.manager.js';

const router = Router();

router.post('', (req, res) => {
    const cart = cartManager.add();

    if (!cart) {
        return res.status(500).json({
            error: 'Error al crear el carrito',
        });
    }

    res.json(cart);
});

router.get('/:id', (req, res) => {
    const { id } = req.params;
    const cart = cartManager.getById(id);

    if (!cart) {
        return res.status(404).json({
            error: 'Carrito no encontrado',
        });
    }

    res.json(cart);
});

router.post('/:cid/product/:pid', (req, res) => {
    const { cid, pid } = req.params;
    const { quantity } = req.body;
    const cart = cartManager.addItemToCart(cid, pid, quantity);

    if (!cart) {
        return res.status(404).json({
            error: 'Error al agregar productos',
        });
    }

    res.json(cart);
});

export default router
