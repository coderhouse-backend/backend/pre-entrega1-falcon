import { Router } from 'express';

import productManager, { Product } from '../manager/product.manager.js';

const router = Router();

router.get('', (req, res) => {
    res.json(productManager.all());
});

router.get('/:id', (req, res) => {
    const { id } = req.params;
    const product = productManager.getById(id);

    if (!product) {
        return res.status(404).json({
            error: 'Producto no encontrado',
        });
    }

    res.json(product);
});

router.post('', (req, res) => {
    const { title, description, code, price, stock, category, thumbnails } = req.body;
    const data = new Product(title, description, code, price, stock, category, thumbnails);
    const product = productManager.add(data);

    if (!product) {
        return res.status(500).json({
            error: 'Error al crear el producto',
        });
    }

    res.json(product);
});

router.put('/:id', (req, res) => {
    const { id } = req.params;
    const { title, description, code, price, stock, status, category, thumbnails } = req.body;

    const data = new Product(title, description, code, price, stock, category, thumbnails, status);
    const product = productManager.update(id, data);

    if (!product) {
        return res.status(500).json({
            error: 'Error al actualizar el producto',
        });
    }

    res.json(product);

});

router.delete('/:id', (req, res) => {
    const { id } = req.params;
    const response = productManager.delete(id);
    if (response !== true) {
        return res.status(500).json({
            error: 'Error al eliminar el producto',
        });
    }

    res.status(200).json({
        message: 'Producto con id: ' + id + ' eliminado',
    });
});

export default router
