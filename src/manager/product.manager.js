import fs from 'fs';

export class Product {
    constructor(title, description, code, price, stock, category, thumbnails, status = true) {
        this.id = 0;
        this.title = title;
        this.description = description;
        this.code = code;
        this.price = price;
        this.status = status;
        this.stock = stock;
        this.category = category;
        this.thumbnails = thumbnails;
    }
}

class ProductManager {
    constructor() {
        this.path = './data/products.json';
        this.products = [];

        if (fs.existsSync(this.path)) {
            try {
                this.products = JSON.parse(fs.readFileSync(this.path, 'utf8'));
            } catch (error) {}
        }
    }

    add(product) {
        if (!this.isValid(product)) {
            console.log('Todos los campos a excepción de las imágenes son obligatorios');
            return null;
        }

        if (this.codeExist(product.code)) {
            console.log('El código ya se encuentra registrado');
            return null;
        }

        product.id = this.newId();
        product.status = true;

        this.products.push(product);
        this.writeFile();

        return product;
    }

    all() {
        return this.products;
    }

    getById(id) {
        id = Number(id);

        if (isNaN(id)) {
            console.log('El id debe de ser un número');
            return null;
        }

        const product = this.products.find((product) => product.id === id);
        if (!product) {
            console.log('No se encontró el producto de id: ' + id);
            return null;
        }

        return product;
    }

    update(id, product) {
        id = Number(id);

        if (isNaN(id)) {
            console.log('El id debe de ser un número');
            return null;
        }

        const index = this.products.findIndex((p) => p.id === id);
        if (index === -1) {
            console.log('No se encontró el producto de id: ' + id);
            return null;
        }

        const currentProduct = this.products[index];

        for (const key in product) {
            if (product.hasOwnProperty(key) && product[key] !== undefined) {
                currentProduct[key] = product[key];
            }
        }

        this.products[index].id = id;

        this.writeFile();

        return this.products[index];
    }

    delete(id) {
        id = Number(id);

        if (isNaN(id)) {
            console.log('El id debe de ser un número');
            return null;
        }

        const index = this.products.findIndex((product) => product.id === id);
        if (index === -1) {
            console.log('No se encontró el producto de id: ' + id);
            return null;
        }

        this.products.splice(index, 1);

        this.writeFile();

        return true;
    }

    isValid(product) {
        return product.title && product.description && product.code && product.price
            && product.stock && product.category;
    }

    codeExist(code) {
        return this.products.some((product) => product.code === code);
    }

    newId() {
        let id = 1;
        if (this.products.length > 0) {
            id = this.products[this.products.length - 1].id + 1;
        }

        return id;
    }

    writeFile() {
        try {
            fs.writeFileSync(
                this.path,
                JSON.stringify(this.products, null, '\t')
            );
        } catch (error) {
            console.error(error);
        }
    }
}

export default new ProductManager();
