import fs from 'fs';

export class Cart {
    constructor(products = []) {
        this.id = 0;
        this.products = products;
    }
}

export class CartItem {
    constructor(product, quantity = 1) {
        this.product = product;
        this.quantity = quantity;
    }
}

class CartManager {
    constructor() {
        this.cartsPath = './data/carts.json';
        this.productsPath = './data/products.json';

        this.carts = [];
        this.products = [];

        if (fs.existsSync(this.cartsPath)) {
            try {
                this.carts = JSON.parse(fs.readFileSync(this.cartsPath, 'utf8'));
            } catch (error) {}
        }

        if (fs.existsSync(this.productsPath)) {
            try {
                this.products = JSON.parse(fs.readFileSync(this.productsPath, 'utf8'));
            } catch (error) {}
        }
    }

    add() {
        const cart = new Cart();
        cart.id = this.newId();

        this.carts.push(cart);
        this.writeFile();

        return cart;
    }

    getById(id) {
        id = Number(id);

        if (isNaN(id)) {
            console.log('El id debe de ser un número');
            return null;
        }

        const cart = this.carts.find((cart) => cart.id === id);
        if (!cart) {
            console.log('No se encontró el carrito de id: ' + id);
            return null;
        }

        return cart;
    }

    addItemToCart(cid, pid) {
        cid = Number(cid);
        if (isNaN(cid)) {
            console.log('El id del carrito debe de ser un número');
            return null;
        }

        pid = Number(pid);
        if (isNaN(pid)) {
            console.log('El id del producto debe de ser un número');
            return null;
        }

        const cartIndex = this.carts.findIndex((cart) => cart.id === cid);
        if (cartIndex === -1) {
            console.log('No se encontró el producto de id: ' + cid);
            return null;
        }

        const productIndex = this.products.findIndex((product) => product.id === pid);
        if (productIndex === -1) {
            console.log('No se encontró el producto de id: ' + pid);
            return null;
        }

        const cart = this.carts[cartIndex];
        let product = this.products[productIndex];

        const productInCartIndex = cart.products.findIndex((p) => p.product.id === pid);
        if (productInCartIndex === -1) {
            cart.products.push(new CartItem(product));
        } else {
            cart.products[productInCartIndex].quantity += 1;
        }

        this.writeFile();

        return cart;

    }

    newId() {
        let id = 1;
        if (this.carts.length > 0) {
            id = this.carts[this.carts.length - 1].id + 1;
        }

        return id;
    }

    writeFile() {
        try {
            fs.writeFileSync(
                this.cartsPath,
                JSON.stringify(this.carts, null, '\t')
            );
        } catch (error) {
            console.error(error);
        }
    }
}

export default new CartManager();
